package PatternMatching;

import SealedClasses.Uber;

public class PatternMatchingDemo {
    public static void main(String[] args) {
        Object car = new Uber();
        
        //Can I get my fare?
        if ( car instanceof Uber uber){
            int fare = uber.calculateFare();
            System.out.println(String.format("Your fare is %s!", fare));
        }
    }
}
