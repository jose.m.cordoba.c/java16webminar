package PatternMatching;

import SealedClasses.Uber;

public class PatternMatchingDemoOld {
    public static void main(String[] args) {
        Object car = new Uber();
        
        //Can I get my fare
        if ( car instanceof Uber ){
            Uber uber = (Uber) car;
            int fare = uber.calculateFare();
            System.out.println(String.format("Your fare is %s!", fare));
        }
    }
}
