package Records;

public class OportunityOld {

    String name;
    int payGrade;
    boolean active;

    public OportunityOld(String name, int payGrade, boolean active) {
        this.name = name;
        this.payGrade = payGrade;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public int getPayGrade() {
        return payGrade;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OportunityOld that = (OportunityOld) o;

        if (payGrade != that.payGrade) return false;
        if (active != that.active) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + payGrade;
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OportunityOld{" +
                "name='" + name + '\'' +
                ", payGrade=" + payGrade +
                ", active=" + active +
                '}';
    }
}
