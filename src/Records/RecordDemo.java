package Records;

public class RecordDemo {

    public static void main(String[] args) {
        Opportunity opportunity1 = new Opportunity("Senior Java Developer", 3, true );

        Opportunity opportunity2 = new Opportunity("Java 16 Developer", 2, true );

        Opportunity opportunity3 = new Opportunity("Java Intern", 1, true );

        Opportunity opportunity4 = new Opportunity("Java Intern", 1, true );

        System.out.println(opportunity1);

        System.out.println(opportunity2.active());

        System.out.println(opportunity3 == opportunity4);

        System.out.println(opportunity3.equals(opportunity4));
    }
}
