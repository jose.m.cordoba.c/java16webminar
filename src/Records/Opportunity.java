package Records;

public record Opportunity(String name, int payGrade, boolean active) {

    public Opportunity {
        if (payGrade < 0){
            throw new IllegalArgumentException();
        }
    }
}
