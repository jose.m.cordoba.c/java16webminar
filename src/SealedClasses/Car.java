package SealedClasses;

public abstract sealed class Car permits Taxi, Uber {

    String name;

    abstract int calculateFare();
}
