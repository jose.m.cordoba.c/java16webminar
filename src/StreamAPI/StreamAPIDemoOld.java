package StreamAPI;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPIDemoOld {

    public static void main(String[] args) {
        List<String> keys = Stream.of("Java", "epam", "Anywhere")
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(keys);
    }
}
