package StreamAPI;

import java.util.List;
import java.util.stream.Stream;

public class StreamAPIDemo {

    public static void main(String[] args) {
        List<String> keys = Stream.of("Java", "epam", "Anywhere")
                .map(String::toUpperCase)
                .toList();

        System.out.println(keys);
    }
}
